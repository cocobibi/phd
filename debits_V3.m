 
clear all
close all
clc


Ue_1=245/(0.3*0.14*1000*60);
Ue_2=300/(0.3*0.14*1000*60);
Ue_3=473/(0.3*0.14*1000*60);
Ue_4=585/(0.3*0.14*1000*60);
Ue_5=698/(0.3*0.14*1000*60);
Ue_6=720/(0.3*0.125*1000*60);
Ue_7=840/(0.3*0.12*1000*60);


xx1=zeros(7,300);


cd G:\1_Steady_flow\Expérience\Matlab_scripts\Analyse\Publi1\5_RXZ_RZZ_TKE_PIV

load UU_Moy2.mat
load wwater_depth2.mat


cd G:\1_Steady_flow\Expérience\Matlab_scripts\Analyse\Publi1\00_debit


%Ue_1
a=225;
b=259;
c=8;


x = wwater_depth2(1,a:b);
y = UU_Moy2(1,a:b);

p = polyfit(x,y,1);

depth1=(wwater_depth2(1,b)-[0:0.5:wwater_depth2(1,b)])-0.5;
x1 = [depth1];
y1 = polyval(p,x1);

xxx1=[wwater_depth2(1,c:b) depth1];
xx1(1,1:length(xxx1))=xxx1;
vel1=[UU_Moy2(1,c:b) y1];


hold on
plot(UU_Moy2(1,:),wwater_depth2(1,:))
plot(vel1,xx1(1,1:length(vel1)))
hold off

%debit

%surface*vitesse
Q1=0.3*0.0005*vel1; %m3/s
QQ1=sum(Q1);

%Ue_1=QQ1/(0.3*0.14);

Ue_1=QQ1/(0.3*0.14);



%Ue_2
w=2
a=200;
b=244;
c=8;

% plot(UU_Moy2(2,a:b),wwater_depth2(2,a:b))

x = wwater_depth2(w,a:b);
y = UU_Moy2(w,a:b);

p = polyfit(x,y,1);

depth1=(wwater_depth2(w,b)-[0:0.5:wwater_depth2(w,b)])-0.5;
x1 = [depth1];
y1 = polyval(p,x1);

xxx1=[wwater_depth2(w,c:b) depth1];
xx1(2,1:length(xxx1))=xxx1;

vel2=[UU_Moy2(w,c:b) y1];


hold on
plot(UU_Moy2(w,:),wwater_depth2(w,:))
plot(vel2,xx1(2,1:length(vel2)))
hold off

%debit

%surface*vitesse

Q1=0.3*0.0005*vel1; %m3/s
QQ1=sum(Q1);

Ue_2=QQ1/(0.3*0.14);



%Ue_3
w=3
a=200;
b=243;
c=8;

% plot(UU_Moy2(w,a:b),wwater_depth2(w,a:b))

x = wwater_depth2(w,a:b);
y = UU_Moy2(w,a:b);

p = polyfit(x,y,1);

depth1=(wwater_depth2(w,b)-[0:0.5:wwater_depth2(w,b)])-0.5;
x1 = [depth1];
y1 = polyval(p,x1);

xxx1=[wwater_depth2(w,c:b) depth1];
xx1(3,1:length(xxx1))=xxx1;
vel3=[UU_Moy2(w,c:b) y1];


hold on
plot(UU_Moy2(w,:),wwater_depth2(w,:))
plot(vel3,xx1(3,1:length(vel3)))
hold off

%debit

%surface*vitesse

Q1=0.3*0.0005*vel1; %m3/s
QQ1=sum(Q1);

Ue_3=QQ1/(0.3*0.14);





%Ue_4
w=4
a=225;
b=255;
c=9;

% plot(UU_Moy2(w,a:b),wwater_depth2(w,a:b))

x = wwater_depth2(w,a:b);
y = UU_Moy2(w,a:b);

p = polyfit(x,y,1);

depth1=(wwater_depth2(w,b)-[0:0.5:wwater_depth2(w,b)])-0.5;
x1 = [depth1];
y1 = polyval(p,x1);

xxx1=[wwater_depth2(w,c:b) depth1];
xx1(4,1:length(xxx1))=xxx1;
vel4=[UU_Moy2(w,c:b) y1];


hold on
plot(UU_Moy2(w,:),wwater_depth2(w,:))
plot(vel4,xx1(4,1:length(vel4)))
hold off

%debit

%surface*vitesse

Q1=0.3*0.0005*vel1; %m3/s
QQ1=sum(Q1);

Ue_4=QQ1/(0.3*0.14);



%Ue_5
w=5
a=225;
b=255;
c=40;

% plot(UU_Moy2(w,a:b),wwater_depth2(w,a:b))

x = wwater_depth2(w,a:b);
y = UU_Moy2(w,a:b);

p = polyfit(x,y,1);

depth1=(wwater_depth2(w,b)-[0:0.5:wwater_depth2(w,b)])-0.5;
x1 = [depth1];
y1 = polyval(p,x1);

xxx1=[wwater_depth2(w,c:b) depth1];
xx1(5,1:length(xxx1))=xxx1;
vel5=[UU_Moy2(w,c:b) y1];


hold on
plot(UU_Moy2(w,:),wwater_depth2(w,:))
plot(vel5,xx1(5,1:length(vel5)))
hold off

%debit

%surface*vitesse

Q1=0.3*0.0005*vel1; %m3/s
QQ1=sum(Q1);

Ue_5=QQ1/(0.3*0.14)



%Ue_6
w=6
a=225;
b=255;
c=40;

% plot(UU_Moy2(w,a:b),wwater_depth2(w,a:b))

x = wwater_depth2(w,a:b);
y = UU_Moy2(w,a:b);

p = polyfit(x,y,1);

depth1=(wwater_depth2(w,b)-[0:0.5:wwater_depth2(w,b)])-0.5;
x1 = [depth1];
y1 = polyval(p,x1);

xxx1=[wwater_depth2(w,c:b) depth1];
xx1(6,1:length(xxx1))=xxx1;
vel6=[UU_Moy2(w,c:b) y1];


hold on
plot(UU_Moy2(w,:),wwater_depth2(w,:))
plot(vel6,xx1(6,1:length(vel6)))
hold off

%debit

%surface*vitesse

Q1=0.3*0.0005*vel1; %m3/s
QQ1=sum(Q1);

Ue_6=QQ1/(0.3*0.125)


%Ue_7
w=7
a=225;
b=255;
c=80;

% plot(UU_Moy2(w,a:b),wwater_depth2(w,a:b))

x = wwater_depth2(w,a:b);
y = UU_Moy2(w,a:b);

p = polyfit(x,y,1);

depth1=(wwater_depth2(w,b)-[0:0.5:wwater_depth2(w,b)])-0.5;
x1 = [depth1];
y1 = polyval(p,x1);

xxx1=[wwater_depth2(w,c:b) depth1];
xx1(7,1:length(xxx1))=xxx1;
vel7=[UU_Moy2(w,c:b) y1]


hold on
plot(UU_Moy2(w,:),wwater_depth2(w,:))
plot(vel7,xx1(7,1:length(vel7)))
hold off

%debit

%surface*vitesse

Q1=0.3*0.0005*vel1; %m3/s
QQ1=sum(Q1);





Ue_7=QQ1/(0.3*0.12)


cd G:\1_Steady_flow\Expérience\Matlab_scripts\Analyse\Publi1\6_Drag_coeff

save ('vel1.mat','vel1')
save ('vel2.mat','vel2')
save ('vel3.mat','vel3')
save ('vel4.mat','vel4')
save ('vel5.mat','vel5')
save ('vel6.mat','vel6')
save ('vel7.mat','vel7')

save ('xx1','xx1')



% to

to_1=0.063;
to_2=0.16;
to_3=1.25;
to_4=1.58;
to_5=2.6;
to_6=1.55;
to_7=1.24;





% %Nombre de Reynold
% 
% Re_1=Ue_1*0.14/(0.000001);
% Re_2=Ue_2*0.14/(0.000001);
% Re_3=Ue_3*0.14/(0.000001);
% Re_4=Ue_4*0.14/(0.000001);
% Re_5=Ue_5*0.14/(0.000001);
% Re_6=Ue_6*0.13/(0.000001);
% Re_7=Ue_7*0.12/(0.000001);
% 
% %Nombre de Reynold *
% 
% Ree_1=sqrt(to_1/1000)*0.000217/(0.000001)
% Ree_2=sqrt(to_2/1000)*0.000217/(0.000001)
% Ree_3=sqrt(to_3/1000)*0.000217/(0.000001)
% Ree_4=sqrt(to_4/1000)*0.000217/(0.000001)
% Ree_5=sqrt(to_5/1000)*0.000217/(0.000001)
% Ree_6=sqrt(to_6/1000)*0.000217/(0.000001)
% Ree_7=sqrt(to_7/1000)*0.000217/(0.000001)
% 
% 
% % Nombre de Froude
% Fr_1=Ue_1/sqrt(9.8*0.14)
% Fr_2=Ue_2/sqrt(9.8*0.14)
% Fr_3=Ue_3/sqrt(9.8*0.14)
% Fr_4=Ue_4/sqrt(9.8*0.14)
% Fr_5=Ue_5/sqrt(9.8*0.14)
% Fr_6=Ue_6/sqrt(9.8*0.125)
% Fr_7=Ue_7/sqrt(9.8*0.12)
% 
% % Froude parameter*
% 
% Frr_1=(to_1)/(1650*0.000217*9.8)
% Frr_2=(to_2)/(1650*0.000217*9.8)
% Frr_3=(to_3)/(1650*0.000217*9.8)
% Frr_4=(to_4)/(1650*0.000217*9.8)
% Frr_5=(to_5)/(1650*0.000217*9.8)
% Frr_6=(to_6)/(1650*0.000217*9.8)
% Frr_7=(to_7)/(1650*0.000217*9.8


hold on
plot(vel1,xx1(1,1:length(vel1)))
plot(vel2,xx1(1,1:length(vel2)))
plot(vel3,xx1(1,1:length(vel3)))
plot(vel4,xx1(1,1:length(vel4)))
plot(vel5,xx1(1,1:length(vel5)))
plot(vel6,xx1(1,1:length(vel6)))
plot(vel7,xx1(1,1:length(vel7)))
hold off
